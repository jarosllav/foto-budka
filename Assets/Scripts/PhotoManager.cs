using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class PhotoManager : MonoBehaviour
{
    [SerializeField] private Transform modelViewer;
    private Assimp.AssimpContext importer;
    private List<Mesh> meshes;
    private Material[] materials;
    private int currentMesh = 0;

    private Vector3 mousePosition;
    private float distance;
    private float zoomMultiplier = 1f;

    // Unity methods
    private void Start() {
        this.importer = new Assimp.AssimpContext();
        
        this.loadModels();
        this.currentMesh = 0;
        this.updateModelViewer();
    }

    private void Update() {
        if(!EventSystem.current.IsPointerOverGameObject()) {
            if(Input.GetMouseButton(0)) {
                Vector3 delta = (Input.mousePosition - mousePosition);
                this.Rotate(delta);
            }

            if(Input.mouseScrollDelta != Vector2.zero) {
                this.Zoom(-1f * zoomMultiplier * Input.mouseScrollDelta.y);
            }
        }

        mousePosition = Input.mousePosition;
    }

    // Public methods
    public void OnBackButton() {
        --this.currentMesh;
        if(this.currentMesh < 0)
            this.currentMesh = this.meshes.Count - 1;
        this.updateModelViewer();
    }
    public void OnForwardButton() {
        ++this.currentMesh;
        if(this.currentMesh > this.meshes.Count - 1)
            this.currentMesh = 0;
        this.updateModelViewer();
    }
    public void OnPhotoButton() {
        Screenshoter.Instance().Take(Camera.main);
    }

    public void Rotate(Vector3 delta) {
        this.modelViewer.Rotate(transform.up, -1f * Vector3.Dot(delta, Camera.main.transform.right), Space.World);
        this.modelViewer.Rotate(Camera.main.transform.right, Vector3.Dot(delta, Camera.main.transform.up), Space.Self);
        this.updateCamera();
    }

    public void ChangeRotation(Vector3 euler) {
        this.modelViewer.localRotation = Quaternion.Euler(euler);
        this.updateCamera();
    }

    public void Zoom(float strength) {
        this.distance += strength;
        this.updateCamera();
    }

    public float GetZoomMultiplier() { return this.zoomMultiplier; }
    public Transform GetModelViewer() { return this.modelViewer; }

    // Private methods
    private void loadModels() {
        string[] modelsFiles = this.getAllModelsFiles(".fbx", ".dae", ".blend", ".3ds", ".ase", ".obj");

        this.meshes = new List<Mesh>();

        foreach(string modelFile in modelsFiles) {
            Assimp.Scene modelScene = this.importer.ImportFile(modelFile, Assimp.PostProcessSteps.JoinIdenticalVertices | Assimp.PostProcessSteps.GenerateNormals | Assimp.PostProcessSteps.Triangulate);

            // Build mesh
            foreach(Assimp.Mesh model in modelScene.Meshes) {
                bool hasNormals = model.HasNormals;
                bool hasUvs = model.HasTextureCoords(0);

                // Mesh data
                Vector3[] vertices = new Vector3[model.VertexCount];
                Vector3[] normals = new Vector3[model.VertexCount];
                Vector2[] uvs = new Vector2[model.VertexCount];

                for(int i = 0; i < model.VertexCount; ++i) {
                    vertices[i] = new Vector3(model.Vertices[i].X, model.Vertices[i].Y, model.Vertices[i].Z);

                    if(hasNormals)
                        normals[i] = new Vector3(model.Normals[i].X, model.Normals[i].Y, model.Normals[i].Z);

                    if(hasUvs)
                        uvs[i] = new Vector2(model.TextureCoordinateChannels[0][i].X, model.TextureCoordinateChannels[0][i].Y);
                }

                Mesh mesh = new Mesh();
                mesh.name = model.Name;
                mesh.vertices = vertices;
                mesh.triangles = model.GetIndices();
                mesh.normals = normals;
                mesh.uv = uvs;

                this.meshes.Add(mesh);
            }
        }
    }

    private string[] getAllModelsFiles(params string[] extensions) {
        string modelsPath = Path.Combine(Application.dataPath, "../Input/");

        if(!Directory.Exists(modelsPath))
            return null;
        
        DirectoryInfo directory = new DirectoryInfo(modelsPath);
        FileInfo[] files = directory.EnumerateFiles()
            .Where(f => extensions.Contains(f.Extension.ToLower()))
            .ToArray();
        
        string[] filesPaths = new string[files.Length];
        for(int i = 0; i < filesPaths.Length; ++i)
            filesPaths[i] = files[i].FullName;

        return filesPaths;
    }

    private void updateModelViewer() {
        this.distance = this.getDistanceToCurrentModel();
        this.zoomMultiplier = Mathf.Sqrt(this.distance);

        Vector3 modelPosition = this.modelViewer.transform.position;
        modelPosition.y -= this.modelViewer.GetComponent<MeshRenderer>().bounds.center.y;
        this.modelViewer.transform.position = modelPosition;

        this.modelViewer.GetComponent<MeshFilter>().sharedMesh = this.meshes[this.currentMesh];
        this.updateCamera();
    }

    private float getDistanceToCurrentModel() {
        Bounds bounds = this.meshes[this.currentMesh].bounds;
        Vector3[] boundsCorners = this.getBoundsCorners(bounds);

        float boundsSphere = boundsCorners.Select(corner => Vector3.Distance(corner, bounds.center)).Max();

        float fov = Mathf.Deg2Rad * Camera.main.fieldOfView;
        return boundsSphere / Mathf.Tan(fov / 2f);
    }

    private void updateCamera() {
        Camera.main.transform.position = this.modelViewer.GetComponent<MeshRenderer>().bounds.center;
        Camera.main.transform.Translate(new Vector3(0, 0, -this.distance));
    }

    private Vector3[] getBoundsCorners(Bounds bounds) {
        return new Vector3[] {
            bounds.min,
            bounds.max,
            new Vector3(bounds.min.x, bounds.min.y, bounds.max.z),
            new Vector3(bounds.min.x, bounds.max.y, bounds.min.z),
            new Vector3(bounds.max.x, bounds.min.y, bounds.min.z),
            new Vector3(bounds.min.x, bounds.max.y, bounds.max.z),
            new Vector3(bounds.max.x, bounds.min.y, bounds.max.z),
            new Vector3(bounds.max.x, bounds.max.y, bounds.min.z)
        };
    }
}
