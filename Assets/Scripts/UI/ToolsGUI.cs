using UnityEngine;
using UnityEngine.UI;

public class ToolsGUI : MonoBehaviour {
    private PhotoManager photoManager;

    [SerializeField] private Slider rotX; 
    [SerializeField] private Slider rotY; 
    [SerializeField] private Slider rotZ;

    private void Awake() {
        this.photoManager = FindObjectOfType<PhotoManager>();    
    }

    private void LateUpdate() {
        Vector3 euler = this.photoManager.GetModelViewer().rotation.eulerAngles; 
        rotX.value = euler.x;
        rotY.value = euler.y;
        rotZ.value = euler.z;
    }

    public void ZoomIn() {
        this.photoManager.Zoom(-1f * this.photoManager.GetZoomMultiplier());
    } 
    public void ZoomOut() {
        this.photoManager.Zoom(1f * this.photoManager.GetZoomMultiplier());
    }

    public void OnChangeRotation(int axis) {
        Transform model = this.photoManager.GetModelViewer();
        Vector3 euler = model.rotation.eulerAngles; 

        if(axis == 0) {
            euler.x = rotX.value;
        }
        else if(axis == 1) {
            euler.y = rotY.value;
        }
        else if(axis == 2) {
            euler.z = rotZ.value;
        }

        this.photoManager.ChangeRotation(new Vector3(rotX.value, rotY.value, rotZ.value));
    }
}