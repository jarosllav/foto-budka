using System.IO;
using UnityEngine;

public enum ScreenFormat {
    PNG, JPG
}

public class Screenshoter
{
    private static Screenshoter instance;
    public static Screenshoter Instance() {
        if(instance == null)
            instance = new Screenshoter();
        return instance;
    }

    private Vector2Int resolution = new Vector2Int(1920, 1080);
    private ScreenFormat screenFormat = ScreenFormat.JPG;
    private string dataPath;

    private Screenshoter() {
        this.dataPath = Path.Combine(Application.dataPath, "../Output/");

        if(!Directory.Exists(this.dataPath))
            Directory.CreateDirectory(this.dataPath);
    }

    // Public methods
    public void Take(Camera camera) {
        Texture2D screenShot = this.capture(camera);
        this.save(screenShot);
    }

    // Private methods
    private Texture2D capture(Camera camera) {
        RenderTexture targetTexture = new RenderTexture(resolution.x, resolution.y, 24);
        camera.targetTexture = targetTexture;

        Texture2D screenShot = new Texture2D(resolution.x, resolution.y, TextureFormat.RGB24, false);
        camera.Render();
        RenderTexture.active = targetTexture;
        screenShot.ReadPixels(new Rect(0, 0, resolution.x, resolution.y), 0, 0);
        camera.targetTexture = null;
        RenderTexture.active = null;
        GameObject.Destroy(targetTexture);

        return screenShot;
    }

    private void save(Texture2D screenShot) {
        byte[] bytes = this.screenFormat == ScreenFormat.PNG ? screenShot.EncodeToPNG() : screenShot.EncodeToJPG();
        string extension = this.screenFormat == ScreenFormat.PNG ? ".png" : ".jpg";
        string path = this.dataPath + "screenshot-" + System.DateTime.Now.ToString("ddMMyyyy-HHmmss") + extension;
        File.WriteAllBytes(path, bytes);
    }
}
